package com.vahagn.androidmidhomework9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class Ex4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ex_4)
    }

    fun calculate(view: View) {
        val first = findViewById<EditText>(R.id.firstNumberInput).text.toString().toDoubleOrNull()
        val second = findViewById<EditText>(R.id.secondNumberInput).text.toString().toDoubleOrNull()

        first?.let { first ->
        second?.let { second ->
            findViewById<TextView>(R.id.resultText).text = when(view.id) {
                R.id.plusButton -> (first + second).toString()
                R.id.minusButton -> (first - second).toString()
                R.id.multiplyButon -> (first * second).toString()
                R.id.divideButton -> if(second != 0.0) (first/second).toString() else "NaN"
                else -> "Unknown bug!"
            }
        } ?: run { invalidInut() }
        } ?: run { invalidInut() }
    }

    private fun invalidInut() {
        Toast.makeText(this, "Invalid input!", Toast.LENGTH_SHORT).show()
    }
}