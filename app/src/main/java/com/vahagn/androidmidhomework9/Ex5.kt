package com.vahagn.androidmidhomework9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlin.math.absoluteValue
import kotlin.math.max

//***********************************************
//*************** IMPORTANT!!! ******************
//***********************************************
//the key moment is to keep a valid double inside the currentNumber TextView

class Ex5 : AppCompatActivity() {
    private val doubleAppearance = "%10.4f"

    private lateinit var actionText: TextView
    private lateinit var signText: TextView
    private lateinit var currentNumber: TextView
    private lateinit var backNumber: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ex_5)

        actionText = findViewById(R.id.actionText)
        signText = findViewById(R.id.signText)
        currentNumber = findViewById(R.id.currentNumber)
        backNumber = findViewById(R.id.backNumber)
    }

    fun clear(view: View) {
        clearAll()
    }

    private fun clearAll() {
        clearCurrentNumber()
        actionText.text = ""
        backNumber.text = ""
    }

    fun action(view: View) {
        actionText.text = (view as Button).text
        if(backNumber.text.toString().toDoubleOrNull() == null) {
            backNumber.text = doubleFormatted(getCurrentNumber())
            clearCurrentNumber()
        }
    }

    fun digit(view: View) {
        currentNumber.text = currentNumber.text.let {
            if(it.equals("0")) (view as Button).text
            else "$it${(view as Button).text}"
        }
    }

    fun backspace(view: View) {
        currentNumber.text = currentNumber.text.subSequence(0, max(0, currentNumber.text.length - 1)).let {
            when(it.length) {
                0 -> "0"
                else -> it
            }
        }
    }
    
    fun calculate(view: View) {
        backNumber.text.toString().toDoubleOrNull()?.let { back ->
        actionText.text.toString().let { action ->
            val current = getCurrentNumber()
            when(action) {
                "+" -> {clearAll(); setCurrentNumber(back + current)}
                "-" -> {clearAll(); setCurrentNumber(back - current)}
                "*" -> {clearAll(); setCurrentNumber(back * current)}
                "%" -> {clearAll(); setCurrentNumber(current / 100 * back)}
                "/" -> if(current != 0.0) {clearAll(); setCurrentNumber(back / current)}
            }
        }
        }
    }

    fun dot(view: View) {
        currentNumber.text.let {
            if(!it.contains('.')) currentNumber.text = "$it."
        }
    }

    fun sign(view: View) {
        signText.text.let {
            signText.text = if(it == "+") "-" else "+"
        }
    }

    private fun getCurrentNumber(): Double {
        return "${signText.text}${currentNumber.text}".toDouble()
    }

    private fun setCurrentNumber(value: Double) {
        signText.text = if(value >= 0) "+" else "-"
        currentNumber.text = doubleFormatted(value.absoluteValue)
    }

    private fun clearCurrentNumber() {
        currentNumber.text = "0"
        signText.text = "+"
    }

    private fun doubleFormatted(value: Double): String {
        return String.format(doubleAppearance, value).trim().let {
            var lastIndex = it.length - 1
            while(it[lastIndex].equals('0')) lastIndex--
            it.substring(0, if(it[lastIndex].equals('.')) lastIndex else lastIndex + 1)
        }
    }
}