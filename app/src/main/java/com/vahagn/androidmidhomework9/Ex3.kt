package com.vahagn.androidmidhomework9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast

class Ex3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ex_3)
    }

    fun login(view: View) {
        val email = findViewById<EditText>(R.id.editTextTextEmailAddress).text.toString()
        val password = findViewById<EditText>(R.id.editTextTextPassword).text.toString()

        if(!(email.contains('@') &&
                        resources.getStringArray(R.array.valid_mails).contains(
                                email.substring(email.indexOf('@') + 1)))) {
            Toast.makeText(this, "Invalid email", Toast.LENGTH_SHORT).show()
            return
        }

        if(password.length < 8 || resources.getStringArray(R.array.invalid_passwords).contains(password)) {
            Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show()
            return
        }

        Toast.makeText(this, "$email\n$password", Toast.LENGTH_SHORT).show()
    }
}